/* global QUnit */

sap.ui.require(["myProfileplugin/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
